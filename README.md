# tolemi-coding-problem

## Project setup
- Please make sure you have Node installed
- From the command line/terminal, `cd` into the root of `tolemi-coding-problem`
- Run the following command to install the project dependencies:
```
npm install
```
- After the project dependencies are installed, please run the following command to run the application: 
```
npm run serve
```
- In the browser, navigate to `127.0.0.1:8080` to view the application
